package org.raml

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.plugins.JavaPlugin
import org.raml.codegen.CodeGeneratorTask


class RamlPlugin implements Plugin<Project>{

    @Override
    void apply(Project project) {
        project.logger?.info("Applying RAML JAX-RS codegen plugin to ${project.name}...")
        project.plugins.apply(JavaPlugin)

        RamlExtension extension = project.extensions.create('raml', RamlExtension, project)
        if (!project.configurations.asMap['raml']) {
            project.configurations.create('raml')
        }

        // Create the JAX-RS code generate task and register it with the project.
        project.tasks.create(name: 'raml-generate', type: CodeGeneratorTask, {
            configuration = project.extensions.raml
        })
        Task generateTask = project.tasks.getByName('raml-generate')
        generateTask.outputs.upToDateWhen {false}
        generateTask.setGroup('Source Generation')
        generateTask.setDescription('Generates JAX-RS annotated Java classes from the provided RAML configuration file(s).')


    }
}