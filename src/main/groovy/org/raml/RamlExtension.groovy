package org.raml

import org.apache.commons.io.FileUtils
import org.gradle.api.InvalidUserDataException
import org.gradle.api.Project

class RamlExtension {

    String basePackageName;

    File outputDirectory;

    File sourceDirectory;

    RamlExtension(Project project) {
        project.logger?.info "Creating RAML JAX-RS code generation extension for project ${project.name}..."
        sourceDirectory = new File(project.getRootDir(), "raml");
        outputDirectory = new File(project.getProjectDir(), "src/main/gen-java")
    }


    Collection<File> getRamlFiles() {
        Set<File> ramlFiles = [] as Set

        if (sourceDirectory) {
            if(!sourceDirectory.isDirectory()) {
                throw new InvalidUserDataException("The provided path doesn't refer to a valid directory: ${sourceDirectory}")
            }

            String[] strings = ["raml"]
            ramlFiles.addAll(FileUtils.listFiles(sourceDirectory, strings, true))
        }

        ramlFiles
    }


}
